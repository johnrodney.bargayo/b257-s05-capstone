from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ToDoItem(models.Model):
    # 'CharField' = String
    # DataTimeField = DateTime datatype
    task_name = models.CharField(max_length = 50)
    description = models.CharField(max_length = 200)
    status = models.CharField(max_length = 50, default = "Pending")
    date_created = models.DateTimeField("Date Created")
    # Adding a user_id to the ToDoItem table, which is a foreign key connected to the User's table
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")

class EventsItem(models.Model):
    event_name = models.CharField(max_length = 50)
    description = models.CharField(max_length = 200)
    status = models.CharField(max_length = 50, default = "Pending")
    event_date= models.DateTimeField("Event's Date")
    user = models.ForeignKey(User, on_delete=models.CASCADE, default="")