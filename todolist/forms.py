from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=20)
    password = forms.CharField(label="Password", max_length=20)

class AddTaskForm(forms.Form):
    task_name = forms.CharField(label="Task Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)

class UpdateTaskForm(forms.Form):
    task_name = forms.CharField(label="Task Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)
    status = forms.CharField(label="Status", max_length=20)

class AddEventForm(forms.Form):
    event_name = forms.CharField(label="Event Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)
    event_date = forms.DateTimeField(
                input_formats=['%d/%m/%Y %H:%M'],
                widget=forms.DateTimeInput(attrs={
                    'class': 'form-control datetimepicker-input',
                    'data-target': '#datetimepicker1'
                })
                )

class UpdateEventForm(forms.Form):
    event_name = forms.CharField(label="Event Name", max_length=50)
    description = forms.CharField(label="Description", max_length=200)
    event_date = forms.DateTimeField(
                input_formats=['%d/%m/%Y %H:%M'],
                widget=forms.DateTimeInput(attrs={
                    'class': 'form-control datetimepicker-input',
                    'data-target': '#datetimepicker1'
                })
                )
    status = forms.CharField(label="Status", max_length=20)

class UpdateProfile(forms.Form):
    username = forms.CharField(label="Username", max_length=20)
    password = forms.CharField(label="Password", max_length=50)
    new_password = forms.CharField(label="Password", max_length=50)
    new_first_name = forms.CharField(label="First Name", max_length=50)
    new_last_name = forms.CharField(label="Last Name", max_length=50)