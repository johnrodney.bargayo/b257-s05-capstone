from django.urls import path
from . import views

# Adding a namespace to this urls.py help django to distinguish this set of routes ferom other urls.py files in other packages.
app_name = 'todolist'
urlpatterns = [
    path('', views.index, name="index"),
    # localhost:8000/todolist/1
    path('<int:todoitem_id>/', views.todoitem, name="viewtodoitem"),
    # localhost:8000/todolist/register
    path('register/', views.register, name="register"),
    # localhost:8000/todolist/update-profile
    path('update-profile/', views.update_profile, name="update_profile"),
    # localhost:8000/todolist/login
    path('login/', views.login_view, name="login"),
    # localhost:8000/todolist/logout
    path('logout/', views.logout_view, name="logout"),
    # localhost:8000/todolist/add_task,
    path('add_task/', views.add_task, name="add_task"),
   # localhost:8000/todolist/1/edit
    path('<int:todoitem_id>/edit', views.update_task, name="update_task"),
    # localhost:8000/todolist/1/delete
    path('<int:todoitem_id>/delete', views.delete_task, name="delete_task"),
    # localhost:8000/todolist/event/1
    path('event/<int:eventitem_id>', views.eventitem, name="vieweventitem"),
    # localhost:8000/todolist/add_event
    path('add_event/', views.add_event, name="add_event"),
    # localhost:8000/todolist/1/delete_event
    path('<int:eventitem_id>/delete_event', views.delete_event, name="delete_event"),
    # localhost:8000/todolist/1/edit_event
    path('<int:eventitem_id>/edit_event', views.update_event, name="update_event"),
]